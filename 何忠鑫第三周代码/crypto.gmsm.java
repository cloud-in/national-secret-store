package org.example;

import cn.superfw.crypto.gmsm.sm2.SM2;
import cn.superfw.crypto.gmsm.sm2.SM2EngineExtend;
import cn.superfw.crypto.gmsm.sm2.SM2KeyPair;
import org.junit.jupiter.api.Test;

public class Main
{
    @Test
    public void testGetKey() {
        System.out.println("压缩：" + SM2.generateSm2Keys(true).getPublicKey());
        System.out.println("非压缩：" + SM2.generateSm2Keys(false).getPublicKey());
    }

    @Test
    public void testSm2() {
        String str = "国密商密";

        // 生成非压缩公钥格式的Key
        SM2KeyPair sm2Keys = SM2.generateSm2Keys(false);

        String pubKey = sm2Keys.getPublicKey();
        String prvKey = sm2Keys.getPrivateKey();

        System.out.println("Private Key: " + prvKey);
        System.out.println("Public Key: " + pubKey);

        System.out.println("\n加密解密测试");
        // 加解密测试(C1C2C3模式)
        System.out.println("----- C1C2C3模式 -----");
        try {
            System.out.println("加密前：" + str);
            String enData = SM2.encrypt(pubKey, str, SM2EngineExtend.CIPHER_MODE_BC);
            System.out.println("加密后：" + enData);
            String deData = SM2.decrypt(prvKey, enData, SM2EngineExtend.CIPHER_MODE_BC);
            System.out.println("解密后：" + deData);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("加解密测试错误");
        }

        // 加解密测试(C1C3C2模式)
        System.out.println("----- C1C3C2模式 -----");
        try {
            System.out.println("加密前：" + str);
            String enData = SM2.encrypt(pubKey, str, SM2EngineExtend.CIPHER_MODE_NORM);
            System.out.println("加密后：" + enData);
            String deData = SM2.decrypt(prvKey, enData, SM2EngineExtend.CIPHER_MODE_NORM);
            System.out.println("解密后：" + deData);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("加解密测试错误");
        }

        System.out.println("\n签名验签测试");
        // 签名和验签测试
        try {
            System.out.println("签名源数据：" + str);
            String signStr = SM2.sign(prvKey, str);
            System.out.println("签名后数据：" + signStr);
            boolean verify = SM2.verify(pubKey, str, signStr);
            System.out.println("签名验证结果：" + verify);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("签名和验签测试错误");
        }
    }

    @Test
    public void testSm2_1() {
        String str = "国密商密";

        // 非压缩公钥，开头不带04的时候
        String pubKey = "BB34D657EE7E8490E66EF577E6B3CEA28B739511E787FB4F71B7F38F241D87F18A5A93DF74E90FF94F4EB907F271A36B295B851F971DA5418F4915E2C1A23D6E";
        String prvKey = "0B1CE43098BC21B8E82B5C065EDB534CB86532B1900A49D49F3C53762D2997FA";

        System.out.println("Private Key: " + prvKey);
        System.out.println("Public Key: " + pubKey);

        System.out.println("\n加密解密测试");
        // 加解密测试(C1C2C3模式)
        System.out.println("----- C1C2C3模式 -----");
        try {
            System.out.println("加密前：" + str);
            String enData = SM2.encrypt(pubKey, str, SM2EngineExtend.CIPHER_MODE_BC);
            System.out.println("加密后：" + enData);
            String deData = SM2.decrypt(prvKey, enData, SM2EngineExtend.CIPHER_MODE_BC);
            System.out.println("解密后：" + deData);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("加解密测试错误");
        }

        // 加解密测试(C1C3C2模式)
        System.out.println("----- C1C3C2模式 -----");
        try {
            System.out.println("加密前：" + str);
            String enData = SM2.encrypt(pubKey, str, SM2EngineExtend.CIPHER_MODE_NORM);
            System.out.println("加密后：" + enData);
            String deData = SM2.decrypt(prvKey, enData, SM2EngineExtend.CIPHER_MODE_NORM);
            System.out.println("解密后：" + deData);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("加解密测试错误");
        }

        System.out.println("\n签名验签测试");
        // 签名和验签测试
        try {
            System.out.println("签名源数据：" + str);
            String signStr = SM2.sign(prvKey, str);
            System.out.println("签名后数据：" + signStr);
            boolean verify = SM2.verify(pubKey, str, signStr);
            System.out.println("签名验证结果：" + verify);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("签名和验签测试错误");
        }
    }
}



package org.example;
import cn.superfw.crypto.gmsm.sm2.SM2EngineExtend;
import cn.superfw.crypto.gmsm.sm2.SM2;
import cn.superfw.crypto.gmsm.sm2.SM2KeyPair;
import cn.superfw.crypto.gmsm.sm3.*;
import cn.superfw.crypto.gmsm.sm4.*;
public class Main {
    public static void main(String[] args) {
        String str = "国密商密";

        // 以非压缩公钥模式生成SM2秘钥对（通常）, 对方是其他语言（比如GO语言）实现时也能支持
        // 压缩模式仅限于数据交互的双方都使用BC库的情况
        SM2KeyPair sm2Keys = SM2.generateSm2Keys(false);

        String pubKey = sm2Keys.getPublicKey();
        String prvKey = sm2Keys.getPrivateKey();

        System.out.println("Private Key: " + prvKey);
        System.out.println("Public Key: " + pubKey);

        System.out.println("\n加密解密测试");
        // 加解密测试(C1C2C3模式)
        System.out.println("----- C1C2C3模式 -----");
        try {
            System.out.println("加密前：" + str);
            String enData = SM2.encrypt(pubKey, str, SM2EngineExtend.CIPHER_MODE_BC);
            System.out.println("加密后：" + enData);
            String deData = SM2.decrypt(prvKey, enData, SM2EngineExtend.CIPHER_MODE_BC);
            System.out.println("解密后：" + deData);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("加解密测试错误");
        }

        // 加解密测试(C1C3C2模式)
        System.out.println("----- C1C3C2模式 -----");
        try {
            System.out.println("加密前：" + str);
            String enData = SM2.encrypt(pubKey, str, SM2EngineExtend.CIPHER_MODE_NORM);
            System.out.println("加密后：" + enData);
            String deData = SM2.decrypt(prvKey, enData, SM2EngineExtend.CIPHER_MODE_NORM);
            System.out.println("解密后：" + deData);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("加解密测试错误");
        }

        System.out.println("\n签名验签测试");
        // 签名和验签测试
        try {
            System.out.println("数据：" + str);
            String signStr = SM2.sign(prvKey, str);
            System.out.println("签名：" + signStr);
            boolean verify = SM2.verify(pubKey, str, signStr);
            System.out.println("验签结果：" + verify);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("签名和验签测试错误");
        }

        System.out.println("SM3 国密商密:"+ SM3.digest("国密商密"));

        String key = SM4.generateKey();
        String input = "国密商密";

        System.out.println("Key：" + key);
        System.out.println("原文：" + input);

        System.out.println();

        System.out.println("ECB模式");

        String output = SM4.encrypt(key, input);
        System.out.println("加密：" + output);
        System.out.println("解密：" + SM4.decrypt(key, output));

        System.out.println();

        System.out.println("CBC模式");
        String iv = "12345678123456781234567812345678";
        output = SM4.encrypt(key, input, iv);
        System.out.println("加密：" + output);
        System.out.println("解密：" + SM4.decrypt(key, output, iv));


    }
}