from Crypto.PublicKey import RSA
#对密钥加密的密码
secret_code = "Unguessable"
#生成一个 2048 位的密钥
key = RSA.generate(2048)

#导出私钥，passphrase 指定了对私钥加密的密码
encrypted_key = key.export_key(passphrase=secret_code, pkcs=8,
                              protection="scryptAndAES128-CBC")

#将私钥保存到文件
file_out = open("rsa_key.bin","wb")
file_out.write(encrypted_key)
file_out.close()

#输出公钥
print(key.publickey().export_key())