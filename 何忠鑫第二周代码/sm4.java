#https://www.mianshigee.com/tutorial/hutool/76b715a34aef8158.md
#Hutool借助Bouncy Castle库可以支持国密算法，以SM4为例

我们首先需要引入Bouncy Castle库：
<dependency>
  <groupId>org.bouncycastle</groupId>
  <artifactId>bcpkix-jdk15on</artifactId>
  <version>1.60</version>
</dependency>
然后可以调用SM4算法，调用方法与其它算法一致：
String content = "test中文";
SymmetricCrypto sm4 = new SymmetricCrypto("SM4");
String encryptHex = sm4.encryptHex(content);
String decryptStr = sm4.decryptStr(encryptHex, CharsetUtil.CHARSET_UTF_8);//test中文
同样我们可以指定加密模式和偏移：
String content = "test中文";
SymmetricCrypto sm4 = new SymmetricCrypto("SM4/ECB/PKCS5Padding");
String encryptHex = sm4.encryptHex(content);
String decryptStr = sm4.decryptStr(encryptHex, CharsetUtil.CHARSET_UTF_8);//test中文

Hutool借助Bouncy Castle库可以支持国密算法
<dependency>
    <groupId>cn.hutool</groupId>
    <artifactId>hutool-all</artifactId>
    <version>5.4.5</version>
</dependency>
<dependency>
    <groupId>org.bouncycastle</groupId>
    <artifactId>bcprov-jdk15on</artifactId>
    <version>1.58</version>
</dependency>



import cn.hutool.crypto.symmetric.SymmetricCrypto;
public class sm4Demo 
{ 
    //key必须是16字节，即128位
    final static String key = "sm4demo123456789";
    //指明加密算法和秘钥
    static SymmetricCrypto sm4 = new SymmetricCrypto("SM4/ECB/PKCS5Padding", key.getBytes());
    //加密为16进制，也可以加密成base64/字节数组
    public static String encryptSm4(String plaintext) 
    { 
        return sm4.encryptHex(plaintext);
    }
    //解密
    public static String decryptSm4(String ciphertext)
    { 
        return sm4.decryptStr(ciphertext);
    }
    public static void main(String[] args) 
    {   
        String content = "hello sm4";
        String plain = encryptSm4(content);
        String cipher = decryptSm4(plain);
        System.out.println(plain + "\n" + cipher);
    }
}


