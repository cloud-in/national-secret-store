Hutool借助Bouncy Castle库可以支持国密算法，以SM3为例
<dependency>
  <groupId>org.bouncycastle</groupId>
  <artifactId>bcpkix-jdk15on</artifactId>
  <version>1.60</version>
</dependency>

然后可以调用SM3算法，调用方法与其它摘要算法一致
Digester digester = DigestUtil.digester("sm3");
//136ce3c86e4ed909b76082055a61586af20b4dab674732ebd4b599eef080c9be
String digestHex = digester.digestHex("aaaaa");