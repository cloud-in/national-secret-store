#include "cryptlib.h"
#include "sm3.h"
#include <iostream>
#include <cryptopp/hex.h>
#include <cryptopp/files.h>

int main(int argc, char* argv[])
{
    using namespace CryptoPP;
    HexEncoder encoder(new FileSink(std::cout));
    std::string msg = "20201303zyb";
    std::string digest;

    SM3 hash;
    hash.Update((const byte*)&msg[0], msg.size());
    digest.resize(hash.DigestSize());
    hash.Final((byte*)&digest[0]);

    std::cout << "Message: " << msg << std::endl;

    std::cout << "Digest: ";
    StringSource(digest, true, new Redirector(encoder));
    std::cout << std::endl;
}